Alarm Server
========================
This is the server for the Alarm application.

1) Running the server
----------------------------------
To run the server for testing, run
$ cd path/to/alarm
$ cd server
$ php -S localhost:8000 web/app_dev.php

To run the tests:
$ phpunit -c app/

To view the doxygen documentation:
Open html/index.html in a web browser

To generate doxygen documentation:
$ doxygen Doxyfile
