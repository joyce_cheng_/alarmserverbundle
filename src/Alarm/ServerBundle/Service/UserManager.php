<?php
namespace Alarm\ServerBundle\Service;

use Alarm\ServerBundle\Entity\AP;
use Alarm\ServerBundle\Entity\Carer;
use Alarm\ServerBundle\Entity\User;

/**
 * UserManger contains functions to make AP and Carers to operate as one class.
 */
class UserManager {
    /**
     * Caches fetched repositories.
     * @var array
     */
    private $repositoryCache = array();

    public function __construct($em) {
        $this->em = $em;
    }

    /**
     * Get repository given repository name. This caches the repository.
     *
     * @param String
     * @return Repository
     */
    public function getRepository($name) {
        if (!isset($this->repositoryCache[$name])) {
            $this->repositoryCache[$name] = $this->em
                ->getRepository("AlarmServerBundle:$name");
        }
        return $this->repositoryCache[$name];
    }

    /**
     * Get class name by a user ID.
     *
     * @param String id
     * @return String className
     */
    public function getClassNameById($id) {
        if ($id == null) return '';
        return strtolower($id[0]) == 'a'? 'AP'
            : (strtolower($id[0]) == 'c'? 'Carer': '');
    }

    /**
     * Get the class name of the opposite type given a user ID.
     *
     * @param id 
     *
     * @return 
     **/
    public function getOppositeClassNameById($id) {
        if ($id == null) return '';
        return strtolower($id[0]) == 'a'? 'Carer'
            : (strtolower($id[0]) == 'c'? 'AP': '');
    }

    /**
     * Returns a user given the user ID. The ID is prepended with A for AP,
     * or C for carer.
     *
     * @param String $id
     * @return Alarm\ServerBundle\Entity\User
     */
    public function getUser($id) {
        $name = $this->getClassNameById($id);
        if ($name != '') {
            return $this->getRepository($name)->find(substr($id, 1));
        }
        return null;
    } 

    /**
     * Test if a given user ID refers to a valid user.
     *
     * @param String $id
     * @return boolean
     */
    public function isValid($id) {
        $name = $this->getClassNameById($id);
        if ($name != '') {
            return !is_null($this->getRepository($name)->find($id));
        }
        return false;
    }

    /**
     * Returns a user object by type
     *
     * @param boolean $type
     * @return Alarm\ServerBundle\Entity\User
     */
    public function getNewByType($type) {
        if ($type === User::AP) {
            return new AP();
        }
        return new Carer();
    }
}
