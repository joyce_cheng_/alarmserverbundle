<?php

namespace Alarm\ServerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Alarm\ServerBundle\Entity\Message;
use Alarm\ServerBundle\Entity\User;

class MessageController extends Controller
{
    /**
     * @Route("/send/{id}", name="_message_send")
     * This action allows clients to send a message via a POST request to 
     * another client. It accepts both a single message or an array of messages.
     * The get parameter refers to the user ID of the sender.
     */
    public function sendAction($id) {
        $request = $this->get('request');
        $content = $request->getContent();
        if ($request->isMethod('POST') && !empty($content)) {
            $inputMessages = json_decode($content, true);
            $inputMessages = $inputMessages["messages"];

            // Make sure sending a single message doesn't crash it.
            if (!is_numeric(key($inputMessages))) {
                $inputMessages = array($inputMessages);
            }

            $userManager = $this->get('userManager');
            if (strtolower($id[0]) === 'a') {
                $ap = $userManager->getUser($id);
                $sender = $ap;
                $receiverType = "carer";
            } else {
                $carer = $userManager->getUser($id);
                $sender = $carer;
                $receiverType = "ap";
            }

            if (is_null($sender)) {
                return new JsonResponse("Sender $id not found", 404);
            }

            $em = $this->getDoctrine()->getManager();
            foreach ($inputMessages as $inputMessage) {
                $$receiverType = $userManager->getUser($inputMessage['receiver_id']);

                if (is_null($$receiverType)) {
                    return new JsonResponse("Receiver ".$inputMessage["receiver_id"]." not found", 404);
                }

                $type = $inputMessage['type'] === Message::OKAY? Message::OKAY: Message::HELP;
                $message = Message::createMessage($type, $sender, $$receiverType)
                    ->setTimestamp($inputMessage['timestamp'])
                    ->setLatitude($inputMessage['latitude'])
                    ->setLongitude($inputMessage['longitude'])
                    ;

                $em->persist($message);
            }

            $em->flush();

            return new JsonResponse(null, 200);
        }
        return new JsonResponse(null, 400);
    }

    /**
     * @Route("/receive/{id}", name="_message_receive")
     * This action allows clients to check if a message is sent to it. 
     * Right now, it doesn't require any security check and anyone can pull
     * and delete the original message since I can't be bothered doing a check
     */
    public function receiveAction($id) {
        $user = $this->get('userManager')->getUser($id);
        if ($user != null) {
        	$messages = $user->getUnreadMessages(false);
        	$em = $this->getDoctrine()->getManager();

        	// send a read message to the original sender.
        	foreach ($messages as $message) {
	        	// Mark the messages as read.
	        	$message->setState(Message::READ);
	        	$em->merge($message);
        	}
        	
        	$em->flush();
            return new JsonResponse(array_values($messages->map(function($m) {return $m->getData();})->toArray()) , 200);
        }
        return new JsonResponse(null, 404);
    }

    /**
     * @Route("/acknowledge/{id}", name="_message_acknowledge")
     * This action allows carers to acknowledge an assisted person's 
     * communication
     */
    public function acknowledgeAction($id) {
        $content = $this->get('request')->getContent();
        if ($this->get('request')->isMethod('POST') && !empty($content)) {
            $request = json_decode($content, true);

            $user = $this->get('usermanager')->getUser($id);
            $target = $this->get('usermanager')->getUser($request['target_id']);
            if (!is_null($user) && !is_null($target)) {
                $message = Message::createMessage(Message::READMSG, $user, $target);
                $em = $this->getDoctrine()->getManager();
                $em->persist($message);
                $em->flush();
                return new JsonResponse("Acknowledged ".$target->getName(), 200);
            }
            return new JsonResponse("User not found", 404);
        }
        return new JsonResponse("Bad request", 400);
    }
}
