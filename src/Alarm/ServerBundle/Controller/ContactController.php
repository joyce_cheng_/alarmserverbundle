<?php

namespace Alarm\ServerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Alarm\ServerBundle\Entity\Message;
use Alarm\ServerBundle\Entity\User;
use Alarm\ServerBundle\Entity\UserRelation;

class ContactController extends Controller
{
    /**
     * @Route("/register", name="_contact_register")
     * This action registers a new user given a post request with the user
     * telephone and type. It responds with the user ID if successful.
     */
    public function registerAction() {
        if ($this->get('request')->isMethod('POST')) {
            $request = $this->get('request')->getContent();
            if (!empty($request)) {
                $request = json_decode($request, true);
                $telephone = $request['telephone'];

                $em = $this->getDoctrine()->getManager();
                if (is_null($em->getRepository('AlarmServerBundle:AP')
                    ->findOneByTelephone($telephone))
                    && is_null($em->getRepository('AlarmServerBundle:Carer')
                    ->findOneByTelephone($telephone))
                ) {
                    $user = $this->get('usermanager')
                        ->getNewByType($request['type'])
                        ->setTelephone($telephone)
                        ->setPassword($request['password'])
                        ->setName($request['name'])
                        ;

                    $em->persist($user);
                    $em->flush();
                    return new JsonResponse(array('userId' => $user->getUserId()), 201);
                }
                return new JsonResponse('User already registered', 412);
            }
        }
        return new JsonResponse(null, 400);
    }

    /**
     * @Route("/login", name="_contact_login")
     * This action registers authenticates a user given a telephone and a password.
     * It responds with the user ID if successful.
     */
    public function loginAction() {
        if ($this->get('request')->isMethod('POST')) {
            $request = $this->get('request')->getContent();
            if (!empty($request)) {
                $request = json_decode($request, true);
                $telephone = $request['telephone'];

                $em = $this->getDoctrine()->getManager();
                if (($user = $em->getRepository('AlarmServerBundle:AP')
                    ->findOneByTelephone($telephone))
                    || ($user = $em->getRepository('AlarmServerBundle:Carer')
                    ->findOneByTelephone($telephone))
                ) {
                    if ($user->isValid($request['password'])) {
                        return new JsonResponse(array('userId' => $user->getUserId()), 200);
                    } else {
                        return new JsonResponse(null, 401);
                    }
                }
            }
        }
        return new JsonResponse(null, 400);
    }

    /**
     * This action returns a list of all pending contact requests.
     *
     * @Route("/requests/{id}", name="_contact_requests")
     */
    public function requestsAction($id) {
        $user = $this->get('usermanager')->getUser($id);
        if (!is_null($user)) {
            return new JsonResponse($user->getPendingRequests(), 200);
        }
        return new JsonResponse(null, 404);
    }

    /**
     * This action accepts a pending user request
     *
     * @Route("/accept/{id}", name="_contact_accept")
     */
    public function acceptAction($id) {
        $content = $this->get('request')->getContent();
        if ($this->get('request')->isMethod('POST') && !empty($content)) {
            $request = json_decode($content, true);

            $user = $this->get('usermanager')->getUser($id);
            $target = $this->get('usermanager')->getUser($request['target_id']);
            if (!is_null($user) && !is_null($target)) {
                if ($user->acceptContact($target)) {
                    $em = $this->getDoctrine()->getManager();
                    $em->merge($user);

                    // Generate a message to tell the original user's app to 
                    // update the message.
                    $message = Message::createMessage(Message::UPDATE, null, $target);
                    $em->persist($message);

                    $em->flush();
                    return new JsonResponse(null, 200);
                }
            }
            return new JsonResponse(null, 404);
        }
        return new JsonResponse(null, 400);
    }

    /**
     * This action accepts a pending user request
     *
     * @Route("/reject/{id}", name="_contact_reject")
     */
    public function rejectAction($id) {
        $content = $this->get('request')->getContent();
        if ($this->get('request')->isMethod('POST') && !empty($content)) {
            $request = json_decode($content, true);

            $user = $this->get('usermanager')->getUser($id);
            $target = $this->get('usermanager')->getUser($request['target_id']);
            if (!is_null($user) && !is_null($target)) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($user->getRelation($target));
                $em->flush();
                return new JsonResponse("Contact rejected", 200);
            }
            return new JsonResponse("User not found", 404);
        }
        return new JsonResponse("Bad request", 400);
    }

    /**
     * @Route("/{id}", name="_contact")
     * This action allows a user to view all his contacts given his id
     */
    public function indexAction($id)
    {
        $user = $this->get('usermanager')->getUser($id);
        if ($user != null) {
            $contacts = array_values($user->getContacts(UserRelation::ACCEPTED)->map(function($contact) {
                return $contact->getData();
            })->toArray());
            return new JsonResponse($contacts, 200);
        }
        return new JsonResponse(null, 404);
    }

    /**
     * @Route("/add/{id}", name="_contact_add")
     * This action a user to add a person based on their telephone number.
     * If the addee is not registered, the adder is told to invite the addee
     * (verbally). Otherwise, the addee is send an invitation (or rather, a
     * message is left on the server for it to fetch.
     */
    public function addAction($id) {
        $request = $this->get('request');
        $content = $request->getContent();
        if ($request->isMethod('POST') && !empty($content)) {
            $request = json_decode($content, true);
            $userManager = $this->get('usermanager');

            $telephone = $request['telephone'];
            $user = $userManager->getUser($id);
            $target = $userManager->getRepository($userManager->getOppositeClassNameById($id))
                ->findOneByTelephone($telephone);
            if (!is_null($target)) {
                if ($user->addContact($target)) {
                    $em = $this->getDoctrine()->getManager();
                    $em->merge($user);

                    // Tell the target that someone added him.
                    $message = Message::createMessage(Message::ADD, $user, $target);
                    $em->persist($message);

                    $em->flush();

                    return new JsonResponse("Contact added", 200);
                }
                return new JsonResponse("User ".$target->getUserId()." already added.", 304);
            }
            return new JsonResponse("User not found", 404);
        }
        return new JsonResponse("Invalid request", 400);
    }

    /**
     * @Route("/delete/{id}", name="_contact_delete")
     * This action allows clients to delete a contact. This is updated on both
     * sides of the relationship. The deletee is also sent an message to update
     * its contacts. This message is not visible to the user.
     */
    public function deleteAction($id) {
        $request = $this->get('request');
        $content = $request->getContent();
        if ($request->isMethod('POST') && !empty($content)) {
            $request = json_decode($content, true);

            $target_id = $request['target_id'];

            // If the first letter is identical, then they must both be either 
            // the carer or the AP, which is illogical.
            if (strtolower(substr($target_id, 0, 1)) != strtolower(substr($id, 0, 1))) {
                $user = $this->get('usermanager')->getUser($id);
                $target = $this->get('usermanager')->getUser($target_id);
                if (!is_null($user) && !is_null($target)) {
                    $relation = $user->getRelation($target);
                    if (!is_null($relation)) {
                        $em = $this->getDoctrine()->getManager();
                        $em->remove($relation);
                        
                        $message = Message::createMessage(Message::UPDATE, null, $target);
                        $em->persist($message);
                        
                        $em->flush();

                        return new JsonResponse("Contact deleted", 200);
                    }
                    return new JsonResponse('You are not a contact of the user', 404);
                }
                return new JsonResponse('User not found', 404);
            }
        }
        return new JsonResponse("Invalid request", 400);
    }

    /**
     * Set a new default contact as given by the post parameter "target_id".
     * ID must be an AP ID and target_id the ID of an accepted contact for this
     * to work.
     *
     * @param id 
     *
     * @return http status code returns:
     * - 304 if no change is needed
     * - 404 if $id does not refer to a valid user
     * - 400 if target_id is not given by POST, or if users of the wrong type is given.
     * - 406 if the users are not accepted contacts
     * - 200 if successful.
     *
     * @Route("/setdefault/{id}", name="_contact_setdefault")
     **/
    public function setDefaultAction($id) {
        $request = $this->get('request');
        $content = $request->getContent();
        if (!$request->isMethod('POST') || empty($content))
            return new JsonResponse("Empty request", 400);

        $request = json_decode($content, true);
        if (!isset($request["target_id"]))
            return new JsonResponse('Post parameter "target_id" missing');
        $target_id = $request['target_id'];

        $userManager = $this->get('usermanager');
        $user = $userManager->getUser($id);
        if (is_null($user)) 
            return new JsonResponse("User $id not found.", 404);
        if ($user->getType() === User::CARER)
            return new JsonResponse("User $id is a carer.", 404);

        $contact = $userManager->getUser($target_id);
        if (is_null($contact)) 
            return new JsonResponse("User $target_id not found.", 404);
        if ($contact->getType() === User::AP)
            return new JsonResponse("User $target_id is an AP.", 404);

        // We can only really do this if user has more than one accepted contact
        // (otherwise, no change would be necessary).
        if ($user->getContacts(UserRelation::ACCEPTED)->count() <= 1)
            return new JsonResponse("No change necessary.", 304);

        if ($user->setDefaultContact($contact)) {
            $em = $this->getDoctrine()->getManager();
            $em->merge($user);
            $em->flush();
            return new JsonResponse("Contact updated.", 200);
        }
        return new JsonResponse("User $target_id is not a valid contact of user $id.");
    }

    /**
     * Get the default contact of a given user.
     *
     * @param id of a valid AP.
     *
     * @return id of the default carer.
     * - 404 if $id does not refer to a valid AP.
     * - 200 if lookup is successful.
     * - 204 if the user has no accepted contacts.
     *
     * @Route("/default/{id}", name="_contact_default")
     **/
    public function getDefaultAction($id) {
        $userManager = $this->get('usermanager');
        $user = $userManager->getUser($id);
        if (is_null($user)) 
            return new JsonResponse("User $id not found.", 404);
        if ($user->getType() === User::CARER)
            return new JsonResponse("", 200);
        if ($user->getContacts(UserRelation::ACCEPTED)->count() == 0)
            return new JsonResponse("", 200);

        return new JsonResponse($user->getDefaultContact()->getUserId(), 200);
    }
}
