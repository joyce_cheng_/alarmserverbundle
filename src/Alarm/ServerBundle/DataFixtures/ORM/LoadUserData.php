<?php
namespace Alarm\ServerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Alarm\ServerBundle\Entity\AP;
use Alarm\ServerBundle\Entity\Carer;
use Alarm\ServerBundle\Entity\Message;
use Alarm\ServerBundle\Entity\User;

class LoadUserData implements FixtureInterface {
    public function load(ObjectManager $manager) {
        // a1 - AP with 2 carers
        $a1 = new AP();
        $a1->setTelephone('11111111')
            ->setName('1')
            ->setPassword('1')
            ;

        // a2 - AP with 1 carer and a pending carer
        $a2 = new AP();
        $a2->setTelephone('22222222')
            ->setName('2')
            ->setPassword('2')
            ;

        // a3 - AP with no carer
        $a3 = new AP();
        $a3->setTelephone('33333333')
            ->setName('3')
            ->setPassword('3')
            ;

        // a4 - AP with pending carer
        $a4 = new AP();
        $a4->setTelephone('77777777')
            ->setName('7')
            ->setPassword('7')
            ;

        // c1 - carer with 2 APs
        $c1 = new Carer();
        $c1->setTelephone('44444444')
            ->setName('4')
            ->setPassword('4')
            ;

        // c2 - carer with 1 AP and a pending AP (confirming side)
        $c2 = new Carer();
        $c2->setTelephone('55555555')
            ->setName('5')
            ->setPassword('5')
            ;

        // c3 - carer with no AP
        $c3 = new Carer();
        $c3->setTelephone('66666666')
            ->setName('6')
            ->setPassword('6')
            ;

        // c8 - carer with no AP
        $c4 = new Carer();
        $c4->setTelephone('88888888')
            ->setName('8')
            ->setPassword('8')
            ;

        foreach (array($a1, $a2, $a3, $a4, $c1, $c2, $c3, $c4) as $user) {
            $manager->persist($user);
        }
        $manager->flush();

        $a1->addContact($c1);
        $a1->addContact($c2);
        $a2->addContact($c1);
        $a2->addContact($c2);
        $a4->addContact($c4);
        $c1->acceptContact($a1);
        $c1->acceptContact($a2);
        $c2->acceptContact($a1);

        $message = Message::createMessage(Message::OKAY, $a1, $c1);

        $manager->merge($message);
        foreach (array($a1, $a2, $a3, $a4, $c1, $c2, $c3, $c4) as $user) {
            $manager->merge($user);
        }
        $manager->flush();
    }
}
