<?php

namespace Alarm\ServerBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class MessageControllerTest extends WebTestCase
{
    public function setUp() {
        $classes = array('Alarm\ServerBundle\DataFixtures\ORM\LoadUserData');
        $this->loadFixtures($classes);
    }

    public function testSend() {
        $client = static::createClient();

        // A message can be sent.
        $client->request('POST', '/m/send/a1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array("messages" => array(
            'receiver_id' => 'c1',
            'type' => 10, // an okay message
            'timestamp' => time(),
            'latitude' => 100,
            'longitude' => 100,
        ))));

        $this->assertTrue($client->getResponse()->isSuccessful());
        $client->request('GET', '/m/receive/c1');
        $this->assertCount(2, json_decode($client->getResponse()->getContent(), true));
        
        // Multiple messages can be sent.
        $client->request('POST', '/m/send/a1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array("messages" => array(array(
            'receiver_id' => 'c1',
            'type' => 10, // an okay message
            'timestamp' => time(),
            'latitude' => 100,
            'longitude' => 100,
        ), array(
            'receiver_id' => 'c1',
            'type' => 10, // an okay message
            'timestamp' => time(),
            'latitude' => 100,
            'longitude' => 100,
        )))));

        $this->assertTrue($client->getResponse()->isSuccessful());
        $client->request('GET', '/m/receive/c1');
        $this->assertCount(2, json_decode($client->getResponse()->getContent(), true));
        $client->request('POST', '/m/send/a10', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array("messages" => array(
            'receiver_id' => 'c1',
            'message' => 'test2',
            'timestamp' => time(),
            'latitude' => 100,
            'longitude' => 100,
        ))));
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('POST', '/m/send/a10', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array("messages" => array(
            'receiver_id' => 'c1',
            'message' => 'test2',
            'timestamp' => time(),
            'latitude' => 100,
            'longitude' => 100,
        ))));
        $this->assertTrue($client->getResponse()->isNotFound());
        $client->request('GET', '/m/send/a1');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testReceive() {
        $client = static::createClient();
        $client->request('GET', '/m/receive/c1');
        $this->assertCount(1, json_decode($client->getResponse()->getContent(), true));
        
        // Make sure messages are not received twice
        $client->request('GET', '/m/receive/c1');
        $this->assertCount(0, json_decode($client->getResponse()->getContent(), true));

    }

    public function testAcknowledge() {
        $client = static::createClient();
        $client->request("POST", "/m/receive/a1");
        $client->request('POST', '/m/acknowledge/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a1',
        )));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->isSuccessful());

        // The ap receives a message.
        $client->request("POST", "/m/receive/a1");
        $this->assertCount(1, json_decode($client->getResponse()->getContent(), true));
    }
}
