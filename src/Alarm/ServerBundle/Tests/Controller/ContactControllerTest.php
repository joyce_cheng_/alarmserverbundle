<?php

namespace Alarm\ServerBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function setUp() {
        $classes = array('Alarm\ServerBundle\DataFixtures\ORM\LoadUserData');
        $this->loadFixtures($classes);
    }

    public function testRegister() {
        $client = static::createClient();

        $client->request('POST', '/c/register', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '0434379993',
            'password'  => 'qwerty',
            'type'      => 0,   // Carer
            'name'      => 'test',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());

        // A telephone number cannot be registered twice.
        $client->request('POST', '/c/register', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '11111111',
            'password'  => 'qwerty',
            'name'      => 'test',
            'type'      => 0,   // Carer
        )));
        $this->assertEquals(412, $client->getResponse()->getStatusCode());

        $client->request('GET', '/c/register');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testRequests() {
        $client = static::createClient();

        $client->request('GET', '/c/requests/a10');
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('GET', '/c/requests/a1');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));

        $client->request('GET', '/c/requests/a2');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));

        $client->request('GET', '/c/requests/c1');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));
        $client->request('GET', '/c/requests/c2');
        $this->assertCount(1,json_decode($client->getResponse()->getContent(), true));
    }

    public function testAccept() {
        $client = static::createClient();

        // Reset to make sure there is no unread messages.
        $client->request('GET', '/m/receive/a4');
        $client->request('POST', '/c/accept/c4', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a4',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());

        // Check that an update message is generated.
        $client->request('GET', '/m/receive/a4');
        $this->assertCount(1, json_decode($client->getResponse()->getContent(), true));

        // Check that for the first accepted contact is set to be the default
        // contact.
        $client->request("GET", "/c/default/a4");
        $this->assertEquals("c4", json_decode($client->getResponse()->getContent(), true));

        // Subsequent additions of contacts do not have the same effect.
        $client->request('POST', '/c/add/a4', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '44444444',
        )));
        $client->request('POST', '/c/accept/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a4',
        )));
        $client->request("GET", "/c/default/a4");
        $this->assertEquals("c4", json_decode($client->getResponse()->getContent(), true));

        // Bad pages
        $client->request('POST', '/c/accept/c10', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a2',
        )));
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('POST', '/c/accept/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a20',
        )));
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('GET', '/c/accept/c1');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testReject() {
        $client = static::createClient();

        $client->request("POST", "/c/reject/c4", array(), array(), array("CONTENT_TYPE" => "application/json"), json_encode(array(
            "target_id" => "a4",
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());

        // Check that the initial user can request again after a rejection.
        $client->request('POST', '/c/add/a4', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '88888888',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testIndex() {
        $client = static::createClient();

        $client->request('GET', '/c/a1');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertCount(2,json_decode($client->getResponse()->getContent(), true));

        $client->request('GET', '/c/a2');
        $this->assertCount(1,json_decode($client->getResponse()->getContent(), true));

        $client->request('GET', '/c/a3');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));
    }

    public function testAdd() {
        $client = static::createClient();

        // Reset to make sure there is no unread messages.
        $client->request('GET', '/m/receive/a3');
        $client->request('POST', '/c/add/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '33333333',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());

        // Check that a add message is generated.
        $client->request('GET', '/m/receive/a3');
        $this->assertCount(1, json_decode($client->getResponse()->getContent(), true));

        $client->request('POST', '/c/add/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '00000000',
        )));
        $this->assertTrue($client->getResponse()->isNotFound());

        // A telephone number cannot be registered twice.
        $client->request('POST', '/c/add/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'telephone' => '33333333',
        )));
        $this->assertEquals(304, $client->getResponse()->getStatusCode());

        $client->request('GET', '/c/add/c1');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testDelete() {
        $client = static::createClient();

        // Relations can be deleted from both sides and it is applied to both sides
        $client->request('POST', '/c/delete/a1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'c1',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());
        $client->request('POST', '/c/a1');
        $this->assertCount(1,json_decode($client->getResponse()->getContent(), true));
        $client->request('POST', '/c/c1');
        $this->assertCount(1,json_decode($client->getResponse()->getContent(), true));

        $client->request('POST', '/c/delete/c1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a2',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());
        $client->request('POST', '/c/a2');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));
        $client->request('POST', '/c/c1');
        $this->assertCount(0,json_decode($client->getResponse()->getContent(), true));

        $client->request('POST', '/c/delete/c10', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'a3',
        )));
        $this->assertTrue($client->getResponse()->isNotFound());
        $client->request('POST', '/c/delete/a3', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'c10',
        )));
        $this->assertTrue($client->getResponse()->isNotFound());

        $client->request('GET', '/c/delete/c1');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSetDefault() {
        $client = static::createClient();

        $client->request('POST', '/c/setdefault/a1', array(), array(), array('CONTENT_TYPE' => 'application/json'), json_encode(array(
            'target_id' => 'c2',
        )));
        $this->assertTrue($client->getResponse()->isSuccessful());

        $client->request('POST', '/c/default/a1');
        $this->assertEquals("c2", json_decode($client->getResponse()->getContent(), true));
    }
}
