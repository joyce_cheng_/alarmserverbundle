<?php
namespace Alarm\ServerBundle\Tests\Unit\Service;

use \Alarm\ServerBundle\Service\UserManager;

class UserManagerTest extends \PHPUnit_Framework_TestCase {
    protected function getEmMock() {
        $em = $this->getMock(
            '\Doctrine\ORM\EntityManager', 
            array('getRepository'),
            array(), '', false
        );
        $em->expects($this->any())
            ->method('getRepository');
        return $em;
    }

    public function testGetClassNameById() {
        $userManager = new UserManager($this->getEmMock());
        $this->assertEquals('', $userManager->getClassNameById(''));
        $this->assertEquals('AP', $userManager->getClassNameById('a100'));
        $this->assertEquals('Carer', $userManager->getClassNameById('c100'));
    }
}
?>
