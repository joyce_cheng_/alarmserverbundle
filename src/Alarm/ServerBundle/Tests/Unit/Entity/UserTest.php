<?php
namespace Alarm\ServerBundle\Test\Unit\Entity;

use Alarm\ServerBundle\Entity\AP;
use Alarm\ServerBundle\Entity\Carer;
use Alarm\ServerBundle\Entity\UserRelation;

class UserTest extends \PHPUnit_Framework_TestCase {
    // An autoincrementable ID for user.
    private $id = 10;

    // Get a persisted user with a unique valid ID.
    protected function getUser($type) {
        $type = 'Alarm\ServerBundle\Entity\\'.$type;
        $user = new $type();
        $reflection = new \ReflectionClass($user);
        $id = $reflection->getProperty('id');
        $id->setAccessible(true);
        $id->setValue($user, $this->id++);
        return $user;
    }

    public function testGetContacts() {
        // Make sure that contacts are always empty initially.
        $ap = $this->getUser('AP');
        $this->assertCount(0, $ap->getContacts());

        $carer = $this->getUser('Carer'); 
        $this->assertCount(0, $carer->getContacts());

        // Assume add contact is correct. If it doesn't, we'll find out in the
        // later test. Just don't use getContacts when testing it.
        $ap->addContact($carer);
        $this->assertCount(1, $ap->getContacts());
        $this->assertCount(1, $carer->getContacts());

        $ap1 = $this->getUser('AP');
        $ap1->addContact($carer);
        $this->assertCount(1, $ap->getContacts());
        $this->assertCount(1, $ap1->getContacts());
        $this->assertCount(2, $carer->getContacts());
    }

    public function testAddContact() {
        // Since we are not testing message since it is a data class, we'll
        // test the message creation as well.
        $ap = $this->getUser('AP');
        $ap1 = $this->getUser('AP');
        $carer = $this->getUser('Carer');
        $carer1 = $this->getUser('Carer');

        // Make sure you can't add users of the same type
        $ap->addContact($ap1);
        $this->assertCount(0, $ap->getContacts());
        $this->assertCount(0, $ap1->getContacts());
        $carer->addContact($carer1);
        $this->assertCount(0, $carer->getContacts());
        $this->assertCount(0, $carer1->getContacts());

        // Contacts are added correctly.
        $ap->addContact($carer);
        $this->assertCount(1, $ap->getContacts());
        $this->assertCount(1, $carer->getContacts());

        // Nothing happens when an existing contact is added.
        $ap->addContact($carer);
        $this->assertCount(1, $ap->getContacts());
        $this->assertCount(1, $carer->getContacts());
    }

    public function testAcceptContact() {
        $ap = $this->getUser('AP');
        $carer = $this->getUser('Carer');
        $ap->addContact($carer);
        $this->assertCount(1, $ap->getContacts(UserRelation::PENDING));
        $this->assertCount(0, $ap->getContacts(UserRelation::ACCEPTED));
        $this->assertCount(1, $carer->getContacts(UserRelation::PENDING));
        $this->assertCount(0, $carer->getContacts(UserRelation::ACCEPTED));

        // The initiator cannot accept a contact.
        $ap->acceptContact($carer);
        $this->assertCount(1, $ap->getContacts(UserRelation::PENDING));
        $this->assertCount(0, $ap->getContacts(UserRelation::ACCEPTED));
        $this->assertCount(1, $carer->getContacts(UserRelation::PENDING));
        $this->assertCount(0, $carer->getContacts(UserRelation::ACCEPTED));

        // The receiver can accept a contact.
        $carer->acceptContact($ap);
        $this->assertCount(1, $carer->getRelations());
        $this->assertCount(1, $ap->getRelations());
        $this->assertSame($carer, $ap->getRelations()->current()->getCarer());
        $this->assertSame($ap, $carer->getRelations()->current()->getAp());
        $this->assertNotNull($ap->getRelation($carer));
        $this->assertNotNull($carer->getRelation($ap));
        $this->assertCount(0, $ap->getContacts(UserRelation::PENDING));
        $this->assertCount(1, $ap->getContacts(UserRelation::ACCEPTED));
        $this->assertCount(0, $carer->getContacts(UserRelation::PENDING));
        $this->assertCount(1, $carer->getContacts(UserRelation::ACCEPTED));
    }
}
