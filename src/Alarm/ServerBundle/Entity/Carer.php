<?php
namespace Alarm\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="carers")
 */
class Carer extends User {
    protected $type = parent::CARER;

    protected $userType = 'c';

    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="UserRelation", mappedBy="carer", cascade={"persist", "remove"})
     */
    protected $apRelations;
    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="carer", cascade={"persist", "remove"})
     */
    protected $messages;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apRelations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add AP Relations
     *
     * @param \Alarm\ServerBundle\Entity\UserRelation $apRelations
     * @return AP
     */
    public function addApRelation(\Alarm\ServerBundle\Entity\UserRelation $apRelations)
    {
        $this->apRelations[] = $apRelations;
        $apRelations->setCarer($this);
    
        return $this;
    }

    /**
     * Remove AP Relations
     *
     * @param \Alarm\ServerBundle\Entity\UserRelation $apRelations
     */
    public function removeApRelation(\Alarm\ServerBundle\Entity\UserRelation $apRelations)
    {
        $this->apRelations->removeElement($apRelations);
        $apRelations->setCarer(null);
    }

    /**
     * Get ap relations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApRelations($state = null)
    {
        return $this->apRelations->filter(function($relation) use ($state) {
            return $state === null || $state === $relation->getState();
        });
    }

    /**
     * Get APs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAps($state = null)
    {
        return $this->apRelations ->filter(function($relation) use ($state) {
            return $state === null || $state === $relation->getState();
        })->map(function($relation) { 
            return $relation->getAp(); 
        });
    }

    /**
     * Add messages
     
     * @param \Alarm\ServerBundle\Entity\Message $messages
     * @return Carer
     */
    public function addMessage(\Alarm\ServerBundle\Entity\Message $messages)
    {
        $messages->setCarer($this);
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Alarm\ServerBundle\Entity\Message $messages
     */
    public function removeMessage(\Alarm\ServerBundle\Entity\Message $messages)
    {
        $messages->setCarer(null);
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function getContacts($state = null) {
        return $this->getAps($state);
    }

    public function getRelations($state = null) {
        return $this->getApRelations($state);
    }

    public function addRelation(UserRelation $relation) {
        return $this->addApRelation($relation);
    }
}
