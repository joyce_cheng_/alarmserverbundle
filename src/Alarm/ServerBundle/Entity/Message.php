<?php
namespace Alarm\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="messages")
 */
class Message {
    // A message is marked as read once it has been downloaded so that a 
    // message is only downloaded once. But it is kept on the server as a record
    const UNREAD = 0;
    const READ   = 1;

    // Type of message.
    const OKAY   = 10; /* A message generated when an AP presses the okay button */
    const READMSG= 11; /* A message generated when an OKAY message is downoaded */
    const UPDATE = 12; /* A message generated to tell users to update their contact cache */
    const ADD	 = 13; /* A message generated when a user adds another as contact */
    const HELP   = 14; /* A message generated when an AP presses the help button */
    const CANCEL = 15; /* A message generated to undo a HELP message. */

    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AP", inversedBy="addMessage")
     */
    protected $ap;

    /**
     * @ORM\ManyToOne(targetEntity="Carer", inversedBy="addMessage")
     */
    protected $carer;

    /**
     * @ORM\Column(type="text")
     */
    protected $timestamp;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $longitude;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $state = self::UNREAD;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $sender;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Message
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
    
    /**
     * Set type
     * @param self::READ || self::OKAY || self::UPDATE
     * @return \Alarm\ServerBundle\Entity\Message
     */
    public function setType($type) {
    	$this->type = $type;
    	return $this;
    }
    
    /**
     * Get type
     * @return self::READ || self::OKAY || self::UPDATE
     */
    public function getType() {
    	return $this->type;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Message
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Message
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set state
     *
     * @param boolean $state
     * @return Message
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return boolean 
     */
    public function getState()
    {
        return (int) $this->state;
    }

    /**
     * Set ap
     *
     * @param \Alarm\ServerBundle\Entity\AP $ap
     * @return Message
     */
    public function setAp(\Alarm\ServerBundle\Entity\AP $ap = null)
    {
        $this->ap = $ap;
    
        return $this;
    }

    /**
     * Get ap
     *
     * @return \Alarm\ServerBundle\Entity\AP 
     */
    public function getAp()
    {
        return $this->ap;
    }

    /**
     * Set carer
     *
     * @param \Alarm\ServerBundle\Entity\Carer $carer
     * @return Message
     */
    public function setCarer(\Alarm\ServerBundle\Entity\Carer $carer = null)
    {
        $this->carer = $carer;
    
        return $this;
    }

    /**
     * Get carer
     *
     * @return \Alarm\ServerBundle\Entity\Carer 
     */
    public function getCarer()
    {
        return $this->carer;
    }

    /**
     * Set sender
     *
     * @param \Alarm\ServerBundle\Entity\User::AP || \Alarm\ServerBundle\Entity\User::CARER
     * @return Message
     */
    public function setSender($sender) {
        $this->sender = $sender;
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Alarm\ServerBundle\Entity\User::AP || \Alarm\ServerBundle\Entity\User::CARER
     */
    public function getSender() {
        return $this->sender;
    }

    public function getData() {
        $sender = $this->getSender() == User::AP? $this->getAp(): $this->getCarer();
        $data = array(
            'sender_id' => is_null($sender)? "": $sender->getUserId(),
            'type' => $this->getType(),
        );
        if (in_array($this->getType(), array(self::OKAY, self::HELP, self::CANCEL, self::READMSG))) {
            $data['latitude'] = $this->getLatitude();
            $data['longitude'] = $this->getLongitude();
            $data['timestamp'] = $this->getTimestamp();
        }

        if (in_array($this->getType(), array(self::ADD))) {
            $data['name'] = $sender->getName();
        }
        return $data;
    }

    /**
     * Factory method to message. 
     *
     * @param type of message: self::OKAY, self::HELP, self::READMSG, self::UPDATE
     * @param initiator of the message. This can be null.
     * @param receiver of the message. This must not be null.
     *
     * @return 
     **/
    public static function createMessage($type, User $initiator = null, User $receiver = null) {
        if (!is_null($initiator) 
            && $initiator->getType() === $receiver->getType()
        ) {
            throw new Exception\SameUserTypeException($initiator);
        }

        if ($receiver->getType() === User::CARER) {
            $ap = $initiator;
            $carer = $receiver;
        } else {
            $ap = $receiver;
            $carer = $initiator;
        }

        $message = new Message();
        $message->setType($type)
            ->setTimestamp(time())
            ->setCarer($carer)
            ->setAp($ap)
            ->setSender(User::getOppositeType($receiver->getType()))
            ;

        switch ($type) {
        case self::OKAY:
            break;
        case self::HELP:
            break;
        case self::READMSG:
            $message->setLatitude(0)
                ->setLongitude(0)
                ;
            break;
        case self::UPDATE:
            $message->setLatitude(0)
                ->setLongitude(0)
                ;
            break;
        case self::ADD:
            $message->setLatitude(0)
                ->setLongitude(0)
                ;
            break;
        case self::CANCEL:
            $message->setLatitude(0)
                ->setLongitude(0)
                ;
            break;
        }

        return $message;
    }
}
