<?php
namespace Alarm\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
abstract class User {
    const CARER = 0;
    const AP = 1;

    protected $type = null; /** The type of user. This should take the value 
                                of the constants declared above. */

    protected $userPrefix = null; /** The prefix for the user ID. */

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     */
    protected $telephone;
    
    /**
     * Set password
     * @param password
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * Authenticate user
     * @param password
     * @return boolean indicating if password is valid.
     */
    public function isValid($password) {
        return $this->password == $password;
    }

    /**
     * Get name
     * @return name
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set name
     * @param name
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return User
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * Get contacts.
     * This either returns a list or carers or a list of APs, depending on the 
     * type of this user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    abstract public function getContacts($state = null);

    /**
     * Test if $contact is an existing contact for $user.
     * Since it is mainly used to avoid duplicating relations, the status of 
     * the relation should not have any effect.
     *
     * @param \Alarm\ServerBundle\Entity\User $user
     * @return boolean
     */
    public function hasContact($contact) {
        return $this->getContacts()->contains($contact);
    }

    /**
     * Adds a UserRelation. Children should implement this to make sure the
     * correct function is called.
     *
     * @param \Alarm\ServerBundle\Entity\UserRelation relation *
     * @return \Alarm\ServerBundle\Entity\User $this
     **/
    abstract function addRelation(UserRelation $relation);

    /**
     * Add a contact
     * 
     * @param \Alarm\ServerBundle\Entity\User $user 
     * @return boolean indicating if the user is added
     */
    public function addContact($contact) {
        // Make sure contacts are not duplicated
        if ($this->hasContact($contact)) return false;

        // Make sure we are adding an carer to an ap or the other way around.
        if ($this->getType() === $contact->getType()) return false;

        $relation = new UserRelation();
        $relation->setInitiator($this->getType());
        $this->addRelation($relation);
        $contact->addRelation($relation);
        return true;
    }

    /**
     * Accept a pending relation with a given user.
     *
     * @param Alarm\ServerBundle\Entity\User
     * @return boolean indicating if that was successful
     */
    public function acceptContact(User $target) {
        $relation = $this->getRelation($target);
        if (!is_null($relation)) {
            // Prevent the initiator from accepting.
            if ($relation->getInitiator() !== $this->getType()) {
                $relation->setState(UserRelation::ACCEPTED);

                if ($relation->getCarer()->getContacts(UserRelation::ACCEPTED)->count() === 1) {
                    $relation->setIsDefault(true);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Get relations.
     * This similar to getContacts(), but the relations are returned instead.
     *
     * @param null || \Alarm\ServerBundle\Entity\User::PENDING  || \Alarm\ServerBundle\Entity\User::ACCEPTED
     * @return \Doctrine\Common\Collections\Collection
     */
    abstract public function getRelations($state = null);

    /**
     * Get relation with a certain user.
     *
     * @param \Alarm\ServerBundle\Entity\User
     * @return \Alarm\ServerBundle\Entity\UserRelation
     */
    public function getRelation(User $target) {
        // By default, there should not be a relation with itself.
        if ($this->getUserId() === $target->getUserId()) {
            return null;
        }

        $relations = $this->getRelations()
            ->filter(function($relation) use ($target) {
                return $relation->getAp()->getUserId() === $target->getUserId() 
                    || $relation->getCarer()->getUserId() === $target->getUserId();
            });
        if ($relations->count() > 0) {
            return $relations->current();
        }

        return null;
    }

    /**
     * Fetch all new messages to be downloaded.
     *
     * @param toArray casts the resulting Message to an array. Default: true
     * @return ArrayCollection
     */
    public function getUnreadMessages($toArray = true) {
        $type = $this->getType();
        $messages = $this->getMessages()
            ->filter(function($message) use ($type) {
                return $message->getState() === Message::UNREAD
                    && $message->getSender() != $type;
            });
            
        return $toArray? $messages->map(function($m) {return $m->getData();}): $messages;
    }

    /**
     * Check if a user has a new message.
     *
     * @return boolean
     */
    public function hasNewMessages() {
        return $this->getUnreadMessages()->count() != 0;
    }

    /**
     * Get the user ID. 
     * This is the key used by clients, where the AP IDs are
     * prepended with an 'a' and carer IDs are prepended with a 'c'.
     * This returns an empty string if the id is not set or set to zero.
     *
     * @return String
     */
    public function getUserId() {
        if ($this->getId() == null) return '';

        return $this->userType . $this->getId();
    }

    /**
     * Get all pending requests and format them as arrays.
     *
     * @return array of contacts
     */
    public function getPendingRequests() {
        $type = $this->getType();
        $relations = $this->getRelations(UserRelation::PENDING)
            ->filter(function($relation) use ($type) {
                return $type !== $relation->getInitiator();
            });
        
        $results = array();
        foreach ($relations as $relation) {
            if ($relation->getCarer()->getUserId() === $this->getUserId()) {
                $user = $relation->getAp();
            } else {
                $user = $relation->getCarer();
            }
            $data = $user->getData();
            unset($data['userId']);
            $results[$user->getUserId()] = $data;
        }
        return $results;
    }

    /**
     * Format this object as an associate map.
     *
     * @return array of data.
     **/
    public function getData() {
        return array(
            'userId'        => $this->getUserId(),
            'telephone' => $this->getTelephone(),
            'name'      => $this->getName(),
        );
    }

    /**
     * Return printable type
     *
     * @param self::AP || self::Carer
     *
     * @return "AP" || "Carer"
     **/
    static public function getTypeString($type) {
        if ($type === self::AP) {
            return "AP";
        } else if ($type === self::Carer) {
            return "carer";
        }
        return "";
    }

    /**
     * Get the "opposite" user type. Return carer if AP is given, AP otherwise
     *
     * @param self::AP || self::CARER
     *
     * @return self::CARER || self::AP
     **/
    static public function getOppositeType($type) {
        if ($type === self::AP) {
            return self::CARER;
        }

        return self::AP;
    }
}
