<?php
namespace Alarm\ServerBundle\Entity\Exception;

class SameUserTypeException extends \Exception {
    public function __construct($user) {
        parent::__construct("Two "
            . Alarm\SererBundle\Entity\User::getTypeString($user->getType())
            . "s provided. An AP and a carer expected."
        );
    }
}

?>
