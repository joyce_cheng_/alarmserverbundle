<?php
namespace Alarm\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="aps")
 */
class AP extends User {
    protected $type = parent::AP;

    protected $userType = 'a';

    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="UserRelation", mappedBy="ap", cascade={"persist", "remove"})
     */
    protected $carerRelations;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="ap", cascade={"persist", "remove"})
     */
    protected $messages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carerRelations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add Carer Relations
     *
     * @param \Alarm\ServerBundle\Entity\Carer $carerRelations
     * @return AP
     */
    public function addCarerRelation(\Alarm\ServerBundle\Entity\UserRelation $carerRelations)
    {
        $this->carerRelations[] = $carerRelations;
        $carerRelations->setAp($this);
    
        return $this;
    }

    /**
     * Remove Carer Relations
     *
     * @param \Alarm\ServerBundle\Entity\Carer $carerRelations
     */
    public function removeCarerRelation(\Alarm\ServerBundle\Entity\UserRelation $carerRelations)
    {
        $this->carerRelations->removeElement($carerRelations);
        $carerRelations->setAP(null);
    }

    /**
     * Get carer relations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarerRelations($state = null)
    {
        return $this->carerRelations->filter(function($relation) use ($state) {
            return $state === null || $state === $relation->getState();
        });
    }

    /**
     * Get carers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarers($state = null)
    {
        return $this->carerRelations->filter(function($relation) use ($state) {
            return $state === null || $state === $relation->getState();
        })->map(function($relation) { 
            return $relation->getCarer(); 
        });
    }

    /**
     * Add messages
     *
     * @param \Alarm\ServerBundle\Entity\Message $messages
     * @return AP
     */
    public function addMessage(\Alarm\ServerBundle\Entity\Message $messages)
    {
        $messages->setAp($this);
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Alarm\ServerBundle\Entity\Message $messages
     */
    public function removeMessage(\Alarm\ServerBundle\Entity\Message $messages)
    {
        $messages->setAp(null);
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function getContacts($state = null) {
        return $this->getCarers($state);
    }

    public function getRelations($state = null) {
        return $this->getCarerRelations($state);
    }

    public function addRelation(UserRelation $relation) {
        return $this->addCarerRelation($relation);
    }

    /**
     * Get the default contact of a user.
     *
     * @return the default contact of a user. It returns null if the user has no accepted contacts.
     **/
    public function getDefaultContact() {
        foreach ($this->getCarerRelations() as $relation) {
            if ($relation->getIsDefault()) {
                return $relation->getCarer();
            }
        }

        return null;
    }

    /**
     * Set the default contact of a user.
     *
     * @param contact to be set
     *
     * @return boolean whether if that was successful.
     */
    public function setDefaultContact(Carer $contact) {
        if ($this->hasContact($contact)) {
            if (!is_null($this->getDefaultContact())) {
                $this->getRelation($this->getDefaultContact())->setIsDefault(false);
            }
            $this->getRelation($contact)->setIsDefault(true);
            return true;
        }
        return false;
    }
}
