<?php
namespace Alarm\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity @ORM\Table(name="user_relations")
 */
class UserRelation {
    // A user relation is pending if it is only accepted on one side. It is
    // accepted otherwise.
    const PENDING   = 0;
    const ACCEPTED  = 1;

    /**
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AP", inversedBy="addCarerRelation")
     */
    protected $ap;

    /**
     * @ORM\ManyToOne(targetEntity="Carer", inversedBy="addApRelation")
     */
    protected $carer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $initiator;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $state = self::PENDING;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isDefault = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param boolean $state
     * @return UserRelation
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return boolean 
     */
    public function getState()
    {
        return (int) $this->state;
    }

    /**
     * Set initiator
     *
     * @param initiator 
     *
     * @return 
     **/
    public function setInitiator($initiator) {
        $this->initiator = $initiator;
    }

    /**
     * Get initiator
     *
     * @return 
     **/
    public function getInitiator() {
        return (int) $this->initiator;
    }

    /**
     * Set ap
     *
     * @param \Alarm\ServerBundle\Entity\AP $ap
     * @return UserRelation
     */
    public function setAp(\Alarm\ServerBundle\Entity\AP $ap = null)
    {
        $this->ap = $ap;
    
        return $this;
    }

    /**
     * Get ap
     *
     * @return \Alarm\ServerBundle\Entity\AP 
     */
    public function getAp()
    {
        return $this->ap;
    }

    /**
     * Set carer
     *
     * @param \Alarm\ServerBundle\Entity\Carer $carer
     * @return UserRelation
     */
    public function setCarer(\Alarm\ServerBundle\Entity\Carer $carer = null)
    {
        $this->carer = $carer;
    
        return $this;
    }

    /**
     * Get carer
     *
     * @return \Alarm\ServerBundle\Entity\Carer 
     */
    public function getCarer()
    {
        return $this->carer;
    }

    /**
     * Set a user relation as the default contact relation. Note that this
     * should only be set on one UserRelation for every AP. There is no check
     * done here since it is resource-hungry.
     *
     * @param boolean indicating if a relation is the default one.
     */
    public function setIsDefault($isDefault) {
        $this->isDefault = $isDefault;
        return $this;
    }

    /**
     * Test if the relation is a default realtion.
     *
     * @return boolean indicating if the relation is a default one.
     **/
    public function getIsDefault() {
        return $this->isDefault;
    }
}

